<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./src/plugins/bootstrap-5.0.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/all.css">
    <title></title>
</head>
<body>
    

    
    <script type="text/javascript" src="../../src/plugins/jquery/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="./src/plugins/bootstrap-5.0.2/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="./assets/js/index.js"></script>
</body>
</html>