<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./src/plugins/bootstrap-5.0.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/all.css">
    <title></title>
</head>
<body>
    
    <div class="container d-flex flex-wrap align-content-center">
        <div class="row area-app">
            <div class="col-12 col-sm-4 d-flex flex-wrap align-content-center">
                <div id="header-app" class="header-app d-flex justify-content-center"><h2>Aluno</h2></div>
                <div class="img-area-app d-flex justify-content-center"><a href="./app/aluno" class="d-flex justify-content-center"><img id="img-app" src="./assets/img/aluno-icon.png"></a></div>
            </div>
            <div class="col-12 col-sm-4 d-flex flex-wrap align-content-center">
                <div class="header-app d-flex justify-content-center"><h2>Sala</h2></div>
                <div class="img-area-app d-flex justify-content-center"><a href="./app/sala" class="d-flex justify-content-center"><img  src="./assets/img/sala-icon.png"></a></div>
            </div>
            <div class="col-12 col-sm-4 d-flex flex-wrap align-content-center">
                <div class="header-app d-flex justify-content-center"><h2>Professor</h2></div>
                <div class="img-area-app"><a href="./app/professor" class="d-flex justify-content-center"><img  src="./assets/img/teacher.png"></a></div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="./src/plugins/jquery/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="./src/plugins/bootstrap-5.0.2/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="./assets/js/index.js"></script>
</body>
</html>